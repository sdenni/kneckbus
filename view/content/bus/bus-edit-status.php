


      <div class="page-heading">
              <h1><i class='fa fa-table'></i> Daftar Bus</h1>
      </div>
            <!-- Page Heading End-->				<!-- Your awesome content goes here -->

      <div class="row">
        <div class="col-md-12">
          <div class="widget">
            <div class="widget-header transparent">
              <h2><strong>Toolbar</strong> CRUD Bus</h2>
              <div class="additional-btn">
                <a href="#" class="hidden reload"><i class="icon-ccw-1"></i></a>
                <a href="#" class="widget-toggle"><i class="icon-down-open-2"></i></a>
                <a href="#" class="widget-close"><i class="icon-cancel-3"></i></a>
              </div>
            </div>
            <div class="widget-content">
              <div class="data-table-toolbar">
                <div class="row">
                  <div class="col-md-4">
                    <form role="form">
                    <input type="text" class="form-control" placeholder="Search...">
                    </form>
                  </div>
                  <div class="col-md-8">
                    <div class="toolbar-btn-action">
                      <a href="bus-add.php" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add new</a>
                    </div>
                  </div>
                </div>
              </div>

              <div class="table-responsive">
                <table data-sortable class="table table-hover table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Kode</th>
                      <th>Tipe</th>
                      <th>Jurusan</th>bootstrap
                      <th>Status</th>
                      <th data-sortable="false">Option</th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td>1</td>
                      <td><strong>BS09281</strong></td>
                      <td>Busway</td>
                      <td>Kebon Kalapa - Cibiru</td>
                      <td><span class="label label-success">Active</span></td>
                      <td>
                        <div class="btn-group btn-group-xs">
                          <a data-toggle="tooltip" title="Active" class="btn btn-default"><i class="fa fa-road"></i></a>
                          <a data-toggle="tooltip" title="Istirahat" class="btn btn-default"><i class="fa fa-flag"></i></a>
                          <a data-toggle="tooltip" title="Mogok" class="btn btn-default"><i class="fa fa-lock"></i></a>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td>1</td>
                      <td><strong>BS09281</strong></td>
                      <td>Busway</td>
                      <td>Kebon Kalapa - Cibiru</td>
                      <td><span class="label label-danger">Mogok</span></td>
                      <td>
                        <div class="btn-group btn-group-xs">
                          <a data-toggle="tooltip" title="Active" class="btn btn-default"><i class="fa fa-road"></i></a>
                          <a data-toggle="tooltip" title="Istirahat" class="btn btn-default"><i class="fa fa-flag"></i></a>
                          <a data-toggle="tooltip" title="Mogok" class="btn btn-default"><i class="fa fa-lock"></i></a>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td>1</td>
                      <td><strong>BS09281</strong></td>
                      <td>Busway</td>
                      <td>Kebon Kalapa - Cibiru</td>
                      <td><span class="label label-warning">Istirahat</span></td>
                      <td>
                        <div class="btn-group btn-group-xs">
                          <a data-toggle="tooltip" title="Active" class="btn btn-default"><i class="fa fa-road"></i></a>
                          <a data-toggle="tooltip" title="Istirahat" class="btn btn-default"><i class="fa fa-flag"></i></a>
                          <a data-toggle="tooltip" title="Mogok" class="btn btn-default"><i class="fa fa-lock"></i></a>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td>1</td>
                      <td><strong>BS09281</strong></td>
                      <td>Busway</td>
                      <td>Kebon Kalapa - Cibiru</td>
                      <td><span class="label label-success">Active</span></td>
                      <td>
                        <div class="btn-group btn-group-xs">
                          <a data-toggle="tooltip" title="Active" class="btn btn-default"><i class="fa fa-road"></i></a>
                          <a data-toggle="tooltip" title="Istirahat" class="btn btn-default"><i class="fa fa-flag"></i></a>
                          <a data-toggle="tooltip" title="Mogok" class="btn btn-default"><i class="fa fa-lock"></i></a>
                        </div>
                      </td>
                    </tr>

                    <tr>
                      <td>1</td>
                      <td><strong>BS09281</strong></td>
                      <td>Busway</td>
                      <td>Kebon Kalapa - Cibiru</td>
                      <td><span class="label label-success">Active</span></td>
                      <td>
                        <div class="btn-group btn-group-xs">
                          <a data-toggle="tooltip" title="Active" class="btn btn-default"><i class="fa fa-road"></i></a>
                          <a data-toggle="tooltip" title="Istirahat" class="btn btn-default"><i class="fa fa-flag"></i></a>
                          <a data-toggle="tooltip" title="Mogok" class="btn btn-default"><i class="fa fa-lock"></i></a>
                        </div>
                      </td>
                    </tr>




                  </tbody>
                </table>
              </div>

              <div class="data-table-toolbar">
                <ul class="pagination">
                  <li class="disabled"><a href="#">&laquo;</a></li>
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
