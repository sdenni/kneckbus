<!-- Left Sidebar Start -->

<?php

session_start();
$pengguna;

if ($_SESSION != null) {
  $pengguna = $_SESSION["pengguna"];
} else {
  $pengguna = "Other";
}


$userAdmin = "admin_user";
$userKneck = "kneck_user";
$userUser = "user_user";

 ?>

<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
       <!-- Search form -->
        <form role="search" class="navbar-form">
            <div class="form-group">
                <input type="text" placeholder="Search" class="form-control">
                <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
            </div>
        </form>
        <div class="clearfix"></div>
        <!--- Profile -->
        <div class="profile-info">
            <div class="col-xs-4">
              <a href="profile.html" class="rounded-image profile-image"><img src="../../images/users/user-100.jpg"></a>
            </div>
            <div class="col-xs-8">
                <div class="profile-text">Welcome <b><?php echo $pengguna;  ?></b></div>
            </div>
        </div>
        <!--- Divider -->
        <div class="clearfix"></div>
        <hr class="divider" />
        <div class="clearfix"></div>
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>

              <li class='has_sub'><a href='javascript:void(0);'><i class='icon-home-3'></i>
                <span>Dashboard</span>
                <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                <ul>
                  <li><a href='../content/dashboard.php'><span>Dashboard - All</span></a></li>
                </ul>
              </li>


                <li class='has_sub'><a href='javascript:void(0);'><i class='icon-home-3'></i>
                  <span>Bus</span>
                  <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                  <ul>
                    <<?php if ($pengguna == $userAdmin): ?>
                      <li><a href='../content/bus-list.php'><span>Daftar bus - Admin</span></a></li>
                      <li><a href='../content/bus-add.php'><span>Tambah Bus - Admin</span></a></li>
                    <?php endif; ?>
                    <?php if ($pengguna == $userKneck): ?>
                        <li><a href='../content/bus-status-edit.php'><span>Update Status - Kneck</span></a></li>
                    <?php endif; ?>

                    <?php if ($pengguna == $userUser): ?>
                        <li><a href='../content/bus-view-all.php'><span>Lihat Daftar Bus - User</span></a></li>
                    <?php endif; ?>
                  </ul>
                </li>



              <li class='has_sub'><a href='javascript:void(0);'><i class='icon-home-3'></i>
                <span>Lajur</span>
                <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                <ul>
                  <li><a href='../content/lajur-tercepat.php'><span>Lajur Tercepat - All</span></a></li>
                  <?php if ($pengguna == $userAdmin): ?>
                    <li><a href='../content/lajur-bus.php'><span>Atur Lajur Bus - Admin</span></a></li>
                  <?php endif; ?>
                </ul>
              </li>

              <?php if ($pengguna != $userKneck): ?>
                <li class='has_sub'><a href='javascript:void(0);'><i class='icon-home-3'></i>
                  <span>Halte</span>
                  <span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
                  <ul>
                    <?php if ($pengguna == $userAdmin): ?>
                      <li><a href='../content/halte-list.php'><span>Daftar Halte - Admin</span></a></li>
                      <li><a href='../content/halte-add.php'><span>Tambah Halte - Admin</span></a></li>
                    <?php endif; ?>
                    <?php if ($pengguna == $userUser): ?>
                        <li><a href='../content/halte-info.php'><span>Lihat Halte - User</span></a></li>
                    <?php endif; ?>
                  </ul>
                </li>
              <?php endif; ?>

            </ul>
        </div>
    <div class="clearfix"></div>
    <div class="clearfix"></div><br><br><br>
</div>
    <div class="left-footer">
        <div class="progress progress-xs">
          <div class="progress-bar bg-green-1" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
            <span class="progress-precentage">80%</span>
          </div>

          <a data-toggle="tooltip" title="See task progress" class="btn btn-default md-trigger" data-modal="task-progress"><i class="fa fa-inbox"></i></a>
        </div>
    </div>
</div>
<!-- Left Sidebar End -->
